<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth', 'role'])->group(function(){
    Route::get('/route-1', function(){
        dd(\Route::current()->getName());
        dd('masuk ke route-1');
    })->name('route-122');
    Route::get('/route-2', function(){
        dd('masuk ke route-2');
    })->name('route-2');
    Route::get('/route-3', function(){
        dd('masuk ke route-3');
    })->name('route-3');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


