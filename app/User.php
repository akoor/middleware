<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use \App\Route;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getUserRole(){
        return $this->role_id;
    }

    public function roles(){
        // table migration tujuan, table pivot, foreign_key tujuan, foreign_key asal)
        return $this->belongsToMany('App\Role', 'user_role', 'user_id', 'role_id');
    }

    public function cek_route($route_name){
        $route_id = Route::where('name', $route_name)->first()->id;
        $roles = $this->roles;

        $cek = false;

        if($roles){
            foreach($roles as $role){
                foreach($role->routes as $route){
                    if(route_id == $route->id){
                        $cek = true;
                    }
                }
            }
        }
        return $cek;
    }
}
