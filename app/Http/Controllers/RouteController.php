<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RouteController extends Controller
{
    public function route_1(){
        return "halaman ini hanya super admin";
    }

    public function route_2(){
        return "halaman ini hanya admin dan super admin";
    }

    public function route_3(){
        return "halaman ini hanya guest, admin dan super admin";
    }


}
