<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function users(){
        // table migration tujuan, table pivot, foreign_key tujuan, foreign_key asal)
        return $this->belongsToMany('App\User', 'user_role', 'role_id', 'user_id');
    }

    public function routes(){
        // table migration tujuan, table pivot, foreign_key tujuan, foreign_key asal)
        return $this->belongsToMany('App\Route', 'route_role', 'role_id', 'route_id');
    }
}
